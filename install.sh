#!/usr/bin/env bash

# -t api token
# -h service home
# -d docker service

get_distribution() {
    lsb_dist=""
    # Every system that we officially support has /etc/os-release
    if [ -r /etc/os-release ]; then
        lsb_dist="$(. /etc/os-release && echo "$ID")"
    fi
    # Returning an empty string here should be alright since the
    # case statements don't act unless you provide an actual value
    echo "$lsb_dist"
}

get_python_version() {
    if which python3 >/dev/null 2>&1; then
        #Python is installed
        python_version=$(python3 --version 2>&1 | awk '{print $2}')
        do_not_update=true
        echo "Python version $python_version is installed."
    else
        #Python is not installed
        echo "No Python executable is found."
    fi
}

get_pip_version() {
    if which pip >/dev/null 2>&1; then
        #Pip is installed
        pip_version=$(pip --version 2>&1 | awk '{print $2}')
        do_not_update=true
        echo "Pip version $pip_version is installed."

    else
        #Python is not installed
        echo "No Pip executable is found."
    fi
}

get_curl_version() {

    if which curl >/dev/null 2>&1; then
        #Curl is installed
        curl_version=$(curl --version 2>&1 | head -n 1 | awk '{print $2}')
        do_not_update= true
        echo "Curl version $curl_version is installed."

    else
        #Python is not installed
        echo "No Curl executable is found."
    fi
}

install() {
    get_python_version
    get_pip_version
    get_curl_version

    # perform some very rudimentary platform detection
    lsb_dist=$(get_distribution)
    lsb_dist="$(echo "$lsb_dist" | tr '[:upper:]' '[:lower:]')"
    echo "$lsb_dist"

    case "$lsb_dist" in
    ubuntu | debian)
        if ! $do_not_update; then
            apt update
        fi
        if [ ! -n "$python_version" ]; then
            apt install -y python3
        fi
        if [ ! -n "$pip_version" ]; then
            apt install -y python3-pip
        fi
        if [ ! -n "$curl_version" ]; then
            apt install -y curl
        fi
        ;;
    centos | redhat)
        if [ ! -n "$curl_version" ]; then
            yum install -y curl
        fi
        if [ ! -n "$python_version" ]; then
            yum groupinstall "Development Tools" -y
            yum install gcc openssl-devel bzip2-devel libffi-devel -y
            curl -O https://www.python.org/ftp/python/3.9.10/Python-3.9.10.tgz
            tar -zxvf Python-3.9.10.tgz
            cd Python-3.9.10 && ./configure prefix=/usr/local/python3
            make && make install
            ln -s /usr/local/python3/bin/python3 /usr/bin/python3
            ln -s /usr/local/python3/bin/pip3 /usr/bin/pip3
        fi
        ;;
    esac

    dist_home="https://gitlab.com/githubcom/dist-cot/-/archive/main/dist-cot-main.tar.gz"
    curl -o cot.tar.gz "$dist_home"

    if [ -d "$ctiply_home" ]; then
        rm -fr $ctiply_home
    fi
    mkdir -p $ctiply_home

    tar -zxvf cot.tar.gz -C $ctiply_home --strip-components 1

    sed -i "s#REPLACE_YOUR_TOKEN#$api_token#g" "$ctiply_home/ctiply.service"
    sed -i "s#REPLACE_YOUR_HOME#$ctiply_home#g" "$ctiply_home/ctiply.service"

    cp -f "$ctiply_home/ctiply.service" /usr/lib/systemd/system/ctiply.service

    pip3 install -r "$ctiply_home/requirements.txt"

    systemctl enable /usr/lib/systemd/system/ctiply.service
    systemctl start ctiply.service
}

ctiply_home="/opt/ctiply"
while getopts ":t:h:d" optname; do
    case "$optname" in
    "t")
        api_token="$OPTARG"
        echo "api token is $OPTARG"
        ;;
    "h")
        ctiply_home="$OPTARG"
        echo "service home is $OPTARG"
        ;;
    "d")
        if [ ! -x /var/lib/docker ]; then
            curl -fsSL https://get.docker.com | bash -s docker
            systemctl enable docker
            systemctl start docker
        fi
        install_from_docker=true
        ;;
    ":")
        echo "No argument value for option $OPTARG"
        ;;
    "?")
        echo "Unknown option $OPTARG"
        ;;
    *)
        echo "Unknown error while processing options"
        ;;
    esac
done

if ! $install_from_docker; then
    install
else
    docker build -t ctiply .
    docker run -d --restart=always -e API_TOKEN=$api_token -e CTIPLE_HOME=$ctiply_home ctiply
fi

