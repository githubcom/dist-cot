FROM python:3.9.12-alpine3.15
ADD https://gitlab.com/githubcom/dist-cot/-/archive/main/dist-cot-main.tar.gz /tmp/cot.tar.gz
RUN mkdir -p $CTIPLY_HOME && \ 
    tar -zxvf /tmp/cot.tar.gz -C $CTIPLY_HOME --strip-components 1 && \
    pip install -r $CTIPLY_HOME/requirements.txt
CMD bash -c 'python $CTIPLY_HOME/main.py && tail -f $CTIPLY_HOME/log.log'
